package bean;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class FruitShow
 */
@WebServlet("/FruitShowSession")
public class FruitShowSession extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//fruitインスタンスを生成
		FruitSession fruit = new FruitSession("いちご",700);
		
		//fruitインスタンスを保存
		HttpSession session = request.getSession();
		session.setAttribute("fruit", fruit);
		
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/showSession.jsp");
		dispatcher.forward(request, response);
	}

}
