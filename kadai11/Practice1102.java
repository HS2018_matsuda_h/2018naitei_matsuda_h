package listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Practice1102 implements ServletContextAttributeListener {

	@Override
	public void attributeAdded(ServletContextAttributeEvent arg0) {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("アプリケーションスコープの利用は禁止です");
		
	}

	@Override
	public void attributeRemoved(ServletContextAttributeEvent arg0) {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	@Override
	public void attributeReplaced(ServletContextAttributeEvent arg0) {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("アプリケーションスコープの利用は禁止なんです");
	}

}
