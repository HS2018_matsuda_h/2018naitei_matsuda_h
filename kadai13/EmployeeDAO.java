package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Employee;

public class EmployeeDAO {
	public List<Employee> findAll() {
		
		Connection conn = null;
		List<Employee> empList = new ArrayList<Employee>();
		
		try {
			
			//JDBCドライバを読み込み
			Class.forName("org.h2.Driver");
			
			//データベースへ接続
			conn = DriverManager.getConnection("jdbc:h2:file:C:/Users/matuda/Desktop/DB/example", "sa", "");
			
			//SELECT分を準備
			String sql = "SELECT ID, NAME, AGE FROM EMPLOYEE";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			
			//SELECTを実行し、結果表を取得
			ResultSet rs = pStmt.executeQuery();
			
			//結果表に格納されたレコードの内容を
			//Employeeインスタンスに背停止、ArrayListインスタンスに追加
			while(rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("NAME");
				int age = rs.getInt("AGE");
				Employee employee = new Employee(id, name, age);
				empList.add(employee);
			}
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
					
				} catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();
					return null;
				}
			}
		}
		return empList;
	}
	public boolean remove(String id) {
		
		Connection conn = null;
		
		try {
			
			//JDBCドライバを読み込み
			Class.forName("org.h2.Driver");
			
			//データベースへ接続
			conn = DriverManager.getConnection("jdbc:h2:file:C:/Users/matuda/Desktop/DB/example", "sa", "");
			
			//delete文を準備
			String sql = "Delete From EMPLOYEE Where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			
			pStmt.setString(1, id);
			
			//deleteを実行
			int result = pStmt.executeUpdate();
			
			if(result != 1) {
				return false;
			}
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		} finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
					
				} catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		return true;
		
	}

}
