package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ex62servlet
 */
@WebServlet("/ex62servlet")
public class ex62servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//乱数を発生
		int random = new java.util.Random().nextInt(10);
		
		if((random % 2) != 0) {
			response.sendRedirect("/ex62/redirected.jsp");
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/forward.jsp");
			dispatcher.forward(request, response);
		}
	
	}

}
